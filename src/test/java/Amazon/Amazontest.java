package Amazon;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Amazontest extends Driverfactory 
{		
	String s,t1,t2,t3;
	@Test(priority=1) 
	public void searchItem() throws InterruptedException 	
	{ 	
		//Actions a=new Actions(driver); 		
		//a.sendKeys(Keys.ENTER).build().perform(); 
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Redmi"+ Keys.ENTER);
		//Reporter.log("Amazon Search Test is Passed", true); 		
		//Thread.sleep(3); 		
		//driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 		
		//t1=driver.findElement(By.xpath("(//a[@class='a-size-base a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal'])[2]")).getText(); 		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 	
		s=driver.findElement(By.xpath("(//a[@class='a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal'])[1]")).getText(); 	
		driver.findElement(By.xpath("(//a[@class='a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal'])[1]")).click(); 	
		Thread.sleep(5000); 	
		Set<String> wid=driver.getWindowHandles(); 	
		for(String id:wid) 		
		{ 		
			driver.switchTo().window(id); 		 	
			} 	
		}
	@Test(priority=2) 		
	public void addToCart() throws InterruptedException  		
	{ 			
		WebDriverWait wait=new WebDriverWait(driver, 50); 		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='add-to-cart-button']"))); 	
		driver.findElement(By.xpath("//input[@id='add-to-cart-button']")).click(); 	
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS); 	
		Assert.assertTrue(driver.findElement(By.xpath("//span[contains(text(),'Added to Cart')]")).isDisplayed(), "Added to Cart element is not displayed"); 	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='nav-cart-text-container']"))); 	
		driver.findElement(By.xpath("//div[@id='nav-cart-text-container']")).click(); 		
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS); 
		Assert.assertTrue(driver.findElement(By.linkText(s)).isDisplayed()); 	
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS); 		
		t2=driver.findElement(By.xpath("//span[@class='a-size-medium a-color-base sc-price sc-white-space-nowrap sc-product-price a-text-bold']")).getText(); 	
		//Assert.assertEquals(t1, t2); 		 
		} 	
	@Test(priority=3) 	
	public void countOfItems() throws InterruptedException 	
	{ 	
		Select quantity=new Select(driver.findElement(By.xpath("//select[@name='quantity']")));      
		quantity.selectByIndex(10);       
		driver.findElement(By.xpath("//input[@name='quantityBox']")).sendKeys("10");        
		Thread.sleep(4000);       
		//driver.findElement(By.xpath("//a[@id='a-autoid-5-announce']")).click(); 	
		driver.findElement(By.linkText("Update")).click(); 	
		Thread.sleep(5000); 	
		} 		
	@Test(priority=4) 	
	public void removeItem() throws InterruptedException 		
	{ 			
		Thread.sleep(10000); 		
		driver.findElement(By.xpath("(//input[@type='submit'])[3]")).click(); 
		Assert.assertTrue(driver.findElement(By.xpath("//div[@class='sc-list-item-removed-msg']")).isDisplayed()); 	
		} 
	} 
	
	


